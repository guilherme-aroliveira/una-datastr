package exercises_05;

import java.util.Scanner;

public class Main {
    
    public static void main(String[] args) {

        int opcao;
        double money;

        Scanner sc = new Scanner(System.in);

        ContaCorrente cc = new ContaCorrente();

        System.out.print("Informe o número da agência: ");
        cc.setAgencia(sc.nextInt());

        System.out.print("Informe o número da conta corrente: ");
        cc.setConta(sc.nextInt());


        do {
            System.out.println("=====================");
            System.out.println("    MENU DE OPÇÕES   ");
            System.out.println("=====================");
            System.out.println("[1] - Depositar");
            System.out.println("[2] - Sacar");
            System.out.println("[3] - Imprimir dados da conta");
            System.out.println("[4] - Sair");

            System.out.print("Digite uma opção: ");
            opcao = sc.nextInt();
            System.out.println("---------------------");

            switch (opcao) {
                case 1:
                    System.out.print("Digite o valor para depósito: R$");
                    money = sc.nextDouble();
                    cc.depositar(money);
                    break;

                case 2:
                    System.out.print("Digite o valor para saque: R$");
                    money = sc.nextDouble();
                    cc.sacar(money);
                    break;

                case 3:
                    System.out.println(cc.exibe());
                    break;
            
                case 4:
                    System.out.println("Fim do programa!");
                    break;

                case 5:
                    System.out.println("Opção válida!!");
                    break;
            }
        } while (opcao != 4);
        sc.close();
    }
}
