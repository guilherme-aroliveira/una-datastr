package exercises_04;

public class Retangulo {
    
    double altura;
    double largura;

    public Retangulo() {

        altura = 0;
        largura = 0;
    }

    public double getAltura() {
        return this.altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public double getLargura() {
        return this.largura;
    }

    public void setLargura(double largura) {
        this.largura = largura;
    }

    public double caulculaArea() {
        return altura * largura;
    }

    public double calculaPerimetro() {
        return 2 * (altura + largura);
    }

    public String exibe() {
        return "altura: " + altura + "\nlargura: " + largura + "\narea: " + caulculaArea() + "\nperimetro: " + calculaPerimetro();
    }
}



