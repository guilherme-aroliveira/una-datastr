package exercises_04;

import java.util.Scanner;

public class Exercicio_02 {
    
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        ContaCorrente cc = new ContaCorrente();

        System.out.print("Informe a agência: ");
        cc.setAgencia(sc.nextInt());

        System.out.print("Informe a conta: ");
        cc.setConta(sc.nextInt());

        System.out.print("Informe um valor para o deposito: $");
        cc.depositar(sc.nextDouble());

        System.out.print("Informe um valor para o saque: $");
        cc.sacar(sc.nextDouble());

        sc.close();

        System.out.println("\n" + cc.exibe());
    }
}
