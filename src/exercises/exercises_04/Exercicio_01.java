package exercises_04;

import java.util.Scanner;

public class Exercicio_01 {
    
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        Retangulo ret = new Retangulo();

        System.out.print("Informe a altura do retangulo: ");
        ret.setAltura(sc.nextDouble());

        System.out.print("Informe a largura do retângulo: ");
        ret.setLargura(sc.nextDouble());

        sc.close();

        System.out.println("\n" + ret.exibe());

        System.out.println();
    }
}
