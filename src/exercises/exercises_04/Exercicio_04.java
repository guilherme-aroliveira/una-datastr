package exercises_04;

import java.util.Scanner;

public class Exercicio_04 {
    
    public static void main(String[] args) {
        
        int x, y, r, opcao;
        double raio;

        Scanner sc = new Scanner(System.in);

        System.out.print("Digite o valor da coordenada x: ");
        x = sc.nextInt();

        System.out.print("Digite o valor da coordenada y: ");
        y = sc.nextInt();

        System.out.print("Digite o valor do raio: ");
        raio = sc.nextDouble();

        Circulo c = new Circulo(x, y, raio);

        do {
            System.out.println("=====================");
            System.out.println("    MENU DE OPÇÕES   ");
            System.out.println("=====================");
            System.out.println("[1] - Mover");
            System.out.println("[2] - Aumentar");
            System.out.println("[3] - Imprimir");
            System.out.println("[4] - Sair");
            System.out.println("=====================");

            System.out.print("Digite um opção: ");
            opcao = sc.nextInt();
            System.out.println("---------------------");

            switch (opcao) {
            case 1:
                System.out.print("Digite o valor para mover a coordenada x: ");
                x = sc.nextInt();
                System.out.print("Digite o valor para mover a coordenada y: ");
                y = sc.nextInt();
                c.moveX(x);
                c.moveY(y);
                break;

            case 2:
                System.out.print("Digite o valor para aumentar o raio do círuclo: ");
                r = sc.nextInt();
                c.aumenta(r);
                break;

            case 3:
                System.out.println(c.exibe());
                break;
        
            case 4:
                System.out.println("Fim do programa!");
                break;
            }
            
        } while (opcao != 4);
        sc.close();

    }
}
