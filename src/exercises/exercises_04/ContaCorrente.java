package exercises_04;

public class ContaCorrente {
    
    private int agencia, conta;
    private double saldo;

    public ContaCorrente () {
    }

    public ContaCorrente (int a, int c) {
        this.agencia = a;
        this.conta = c;
        this.saldo = 0;
    }

    public int getAgencia() {
        return this.agencia;
    }

    public void setAgencia(int agencia) {
        this.agencia = agencia;
    }

    public int getConta() {
        return this.conta;
    }

    public void setConta(int conta) {
        this.conta = conta;
    }

    public void sacar(double valor) {
        
        if (valor > saldo) {
            System.out.println("Valor inválido para saque!!");
        }
        else {
            saldo = saldo - valor;
        }
    }

    public void depositar(double valor) {
        saldo = saldo + valor;
    }

    public double consultarSaldo() {
        return saldo;
    }

    public String exibe() {
        return "Agência: " + agencia
        + "\nConta: " + conta
        + "\nSaldo: R$" + consultarSaldo();
    }
}
