package exercises_04;

public class Circulo {
    
    private int x, y;
    private double raio;

    public Circulo(int x, int y, double raio) {
        this.x = x;
        this.y = y;
        this.raio = raio;
    }

    public Circulo() {
        this(0,0,0);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public double getRaio() {
        return raio;
    }

    public void setRaio(double raio) {
        this.raio = raio;
    }

    public double area() {
        return Math.PI * Math.pow(raio, 2);
    }

    public double perimetro() {
        return 2 * Math.PI * raio;
    }

    public void moveX(int x1) {
        this.x = this.x + x1;
    }

    public void moveY(int y1) {
        this.y = this.y + y1;
    }

    public void aumenta(int r) {
        this.raio = this.raio + r;
    }

    public String exibe() {
        return "Centro: (" + this.x + ", " + this.y + ")"
            + "\nRaio: " + String.format("%.2f", this.raio)
            + "\nÁrea: " + String.format("%.2f", this.area())
            + "\nPerímetro: " + String.format("%.2f", this.perimetro());
    }
}
