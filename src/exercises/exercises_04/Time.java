package exercises_04;

public class Time {
    
    private int hora, minutos, segundos;
    

    public Time() {
        hora = 0;
        minutos = 0;
        segundos = 0;
    }

    public Time(int hora) {
        this.hora = hora;
        minutos = 0;
        segundos = 0;
    }

    public Time(int hora, int minutos) {
        this.hora = hora;
        this.minutos = minutos;
        segundos = 0;
    }

    public Time(int hora, int minutos, int segundos) {
        this.hora = hora;
        this.minutos = minutos;
        this.segundos = segundos;
    }
    
    public int getHora() {
        return this.hora;
    }

    public void setHora(int hora) {
        if (hora >= 0 && hora <= 23)
        {
            this.hora = hora;
        }
        else {
            this.hora = 0;
        }
    }

    public int getMinutos() {
        return this.minutos;
    }

    public void setMinutos(int minutos) {

        if (minutos >= 0 && minutos <= 59)
        {
            this.minutos = minutos;
        }
        else {
            this.minutos = 0;
        }
    }

    public int getSegundos() {
        return this.segundos;
    }

    public void setSegundos(int segundos) {

        if (segundos >= 0 && segundos <= 59)
        {
            this.segundos = segundos;
        }
        else {
            this.segundos = 0;
        }
    }

    public void setTime(int hora, int minutos, int segundos) {
        setHora(hora);
        setMinutos(minutos);
        setSegundos(segundos);
    }
    
    public String tempo() {
        return hora + ":" + minutos + ":" + segundos;
    }
}
