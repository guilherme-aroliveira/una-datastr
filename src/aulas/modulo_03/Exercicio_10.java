package aulas.modulo_03;

import java.util.Scanner;

public class Exercicio_10 {
    
    public static void main(String[] args) {
        
        double raio, comprimento, area, volume;

        Scanner sc = new Scanner(System.in);

        System.out.print("Digite o valor do raio: ");
        raio = sc.nextDouble();
        sc.close();

        comprimento = 2 * Math.PI * raio;
        area = Math.PI * Math.pow(raio, 2);
        volume = (4 / 3) * (Math.PI * Math.cbrt(raio));

        System.out.println("Comprimento: " + comprimento);
        System.out.println("Área: " + area);
        System.out.println("Volume: " + volume);
    }
}
