package aulas.modulo_03;

import java.util.Scanner;

public class Exercicio_06 {
    
    public static void main(String[] args) {
        
        double tempC, tempF;

        Scanner sc = new Scanner(System.in);

        System.out.print("Digite a temperatura em Celsius: ");
        tempC = sc.nextDouble();

        sc.close();

        tempF = (tempC * 1.8) + 32;

        System.out.println("Temperatura em Fahrenheit: " + tempF);
    }
}
