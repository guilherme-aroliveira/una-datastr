package aulas.modulo_03;

import java.util.Scanner;

public class Exercicio_07 {
    
    public static void main(String[] args) {
        
        double salarioMinimo, salarioAtual, salarios;

        Scanner sc = new Scanner(System.in);

        System.out.print("Digite o atual salário mínimo: R$");
        salarioMinimo = sc.nextDouble();
        System.out.print("Digite o salário do funcionário: R$");
        salarioAtual = sc.nextDouble();
        sc.close();

        salarios = salarioAtual / salarioMinimo;

        System.out.printf("Você ganha %.0f ", salarios);
        System.out.print("salários minímos");
        System.out.println();
    }
}
