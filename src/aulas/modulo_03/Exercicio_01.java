package aulas.modulo_03;

import java.util.Scanner;

public class Exercicio_01 {
    
    public static void main(String[] args) {

        double nota1, nota2, nota3, media;

        Scanner sc = new Scanner(System.in);
        
        System.out.print("Nota 1: ");
        nota1 = sc.nextDouble();
        System.out.print("Nota 2: ");
        nota2 = sc.nextDouble();
        System.out.print("Nota 3: ");
        nota3 = sc.nextDouble();

        sc.close();

        media = (nota1 + nota2 + nota3) / 3;

        System.out.println("Média: " + media);
    }
}
