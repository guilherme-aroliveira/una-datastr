package aulas.modulo_03;

import java.util.Scanner;

public class Exercicio_08 {
    
    public static void main(String[] args) {
        
        double peso, ganhouPeso, perdeuPeso;

        Scanner sc = new Scanner(System.in);

        System.out.print("Digite o seu peso: ");
        peso = sc.nextDouble();
        sc.close();

        ganhouPeso = peso + ((peso * 15 ) / 100);
        perdeuPeso = peso + ((peso * 20) / 100);

        System.out.printf("Engordou: %.1f", ganhouPeso);
        System.out.printf("\nEmagreceu: %.1f", perdeuPeso);

        System.out.println();
    }
}
