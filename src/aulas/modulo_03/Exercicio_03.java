package aulas.modulo_03;

import java.util.Scanner;

public class Exercicio_03 {
    
    public static void main(String[] args) {
        
        double cotacaoDolar, dolares, valorReal;

        Scanner sc = new Scanner(System.in);

        System.out.print("Informe a cotação do dólar: $");
        cotacaoDolar = sc.nextDouble();
        System.out.print("Informe quanto de dolares você possui: ");
        dolares = sc.nextDouble();

        sc.close();

        valorReal = dolares * cotacaoDolar;

        System.out.printf("O valor em real: $%.2f", valorReal);
        System.out.println("\n");
    }
}
