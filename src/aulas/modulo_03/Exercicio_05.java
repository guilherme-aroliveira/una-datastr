package aulas.modulo_03;

import java.util.Scanner;

public class Exercicio_05 {
    
    public static void main(String[] args) {
        
        double area, diagonalMaior, diagonalMenor;

        Scanner sc = new Scanner(System.in);

        System.out.print("Digite a diagonal maior: ");
        diagonalMaior = sc.nextDouble();
        System.out.print("Digite a diagonal menor: ");
        diagonalMenor = sc.nextDouble();

        sc.close();

        area = (diagonalMaior * diagonalMenor) / 2;

        System.out.println("Area do losango: " + area);
    }
}
