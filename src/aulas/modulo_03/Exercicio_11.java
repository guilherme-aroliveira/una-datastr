package aulas.modulo_03;

import java.util.Scanner;

public class Exercicio_11 {

    public static void main(String[] args) {
        
        int a, b, c, delta;
        double x1, x2, raiz;

        Scanner sc = new Scanner(System.in);

        System.out.print("Digite o coeficiente [a]: ");
        a = sc.nextInt();
        System.out.print("Digite o coeficiente [b]: ");
        b = sc.nextInt();
        System.out.print("Digite o coeficiente [c]: ");
        c = sc.nextInt();
        sc.close();

        delta = (b * b) - (4 * a * c);
        raiz = Math.sqrt(Math.abs(delta));
        x1 = (-b + raiz) / (2 * a);
        x2 = (-b - raiz) / (2 * a);
        
        System.out.println("O primeiro valor da raiz [x1]: " + x1);
        System.out.println("O segundo valor da raiz [x2]: " + x2);
    }
}
