package aulas.modulo_03;

import java.util.Scanner;

public class Exercicio_02 {
    
    public static void main(String[] args) {
        
        int anoNascimento, anoAtual, idade, idade50;

        Scanner sc = new Scanner(System.in);

        System.out.print("Informe o ano de nascimento: ");
        anoNascimento = sc.nextInt();
        System.out.print("Informe o ano atual: ");
        anoAtual = sc.nextInt();

        sc.close();

        idade = anoAtual - anoNascimento;
        idade50 = 2050 - anoNascimento;
        
        System.out.println("Sua idade é: " + idade);
        System.out.println("Sua idade em 2050 será: " + idade50);
    }
}
