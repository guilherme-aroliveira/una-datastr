package aulas.modulo_03;

import java.util.Scanner;

public class Exercicio_09 {
    
    public static void main(String[] args) {
        
        double catetoA, catetoB, hipotenusa;

        Scanner sc = new Scanner(System.in);

        System.out.print("Digite o valor do cateto adjacente: ");
        catetoA = sc.nextDouble();
        System.out.print("Digite o cateto oposto: ");
        catetoB = sc.nextDouble();
        sc.close();

        hipotenusa = Math.sqrt((Math.pow(catetoA, 2) + Math.pow(catetoB, 2)));

        System.out.println("O valor da hipotenusa é: " + hipotenusa);
    }
}
