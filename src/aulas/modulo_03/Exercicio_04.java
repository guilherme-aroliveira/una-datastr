package aulas.modulo_03;

import java.util.Scanner;

public class Exercicio_04 {
    
    public static void main(String[] args) {
        
        double salario, aumento, novoSalario;

        Scanner sc = new Scanner(System.in);

        System.out.print("Informe o seu salário: R$");
        salario = sc.nextDouble();
        sc.close();

        aumento = salario * 25 / 100;
        novoSalario = salario + aumento;

        System.out.printf("Seu novo salario: R$%.2f", novoSalario);
        System.out.println("\n");
    }
}
