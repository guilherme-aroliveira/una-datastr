package aulas.modulo_04;

import java.util.Scanner;

public class Exercicio_09 {
    
    public static void main(String[] args) {
        
        int idade;
        char sexo;

        Scanner sc = new Scanner(System.in);

        System.out.print("Digite a sua idade: ");
        idade = sc.nextInt();
        System.out.print("Informe o seu sexo - [M] ou [F]: ");
        sexo = sc.next().charAt(0);
        sc.close();

        if (sexo == 'M' || sexo == 'm') {

            if (idade <= 15) {
                System.out.println("Mensalidade R$60,00");
            }
            else if (idade >= 16 && idade <= 18) {
                System.out.println("Mensalidade R$75,00");
            }
            else if (idade >= 19 && idade <= 30) {
                System.out.println("Mensalidade R$90,00");
            }
            else if (idade >= 31 && idade <= 40) {
                System.out.println("Mensalidade R85,00");
            }
            else {
                System.out.println("Mensalidade R$80,00");
            }
        }
        else if (sexo == 'F' || sexo == 'f') {
            
            if (idade <= 18) {
                System.out.println("Mensalidade R$60,00");
            }
            else if (idade >= 19 && idade <= 25) {
                System.out.println("Mensalidade R$90,00");
            }
            else if (idade >= 26 && idade <= 40) {
                System.out.println("Mensalidade R$85,00");
            }
            else {
                System.out.println("Mensalidade R$80,00");
            }
        }
    }
}
