package aulas.modulo_04;

import java.util.Scanner;

public class Exercicio_04 {
    
    public static void main(String[] args) {
        
        double imc, peso, altura;
        
        Scanner sc = new Scanner(System.in);

        System.out.print("Digite o seu peso [kg]: ");
        peso = sc.nextDouble();
        System.out.print("Digite a sua altura [m]: ");
        altura = sc.nextDouble();

        sc.close();

        imc = peso / (altura * altura);

        if (imc < 20) {
            System.out.println("Abaixo do peso");
        }
        else if (imc >= 20 && imc < 25) {
            System.out.println("Peso normal");
        }
        else if (imc >= 25 && imc < 30) {
            System.out.println("Sobre peso");
        }
        else if (imc >= 30 && imc < 40) {
            System.out.println("Obeso");
        }
        else {
            System.out.println("Sobre Mórbido");
        }
    }
}
