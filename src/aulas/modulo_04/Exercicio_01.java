package aulas.modulo_04;

import java.util.Scanner;

public class Exercicio_01 {
    
    public static void main(String[] args) {
        
        double nota1, nota2, media;

        Scanner sc = new Scanner(System.in);

        System.out.print("Digite a primeira nota: ");
        nota1 = sc.nextDouble();
        System.out.print("Digite a segunda nota: ");
        nota2 = sc.nextDouble();
        sc.close();

        media = (nota1 + nota2) / 2;

        if (media >= 7) {
            System.out.println("Aprovado");
        }
        else {
            System.out.println("Reprovado");
        }
    }
}
