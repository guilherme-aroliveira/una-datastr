package aulas.modulo_04;

import java.util.Scanner;

public class Exercicio_05 {
    
    public static void main(String[] args) {
        
        String planeta;
        double v0, t, vT, hT;

        Scanner sc = new Scanner(System.in);

        System.out.print("Em qual planeta deseja jogar a bola: ");
        planeta = sc.next();
        System.out.print("Informe a velocidade da bola: ");
        v0 = sc .nextDouble();
        System.out.print("Informe o instante de tempo: ");
        t = sc.nextDouble();

        if (planeta.equals("Mercúrio")) {
            vT = v0 - (3.7 * t);
            hT = (v0 * t) - ((3.7 * (t * t)) / 2);
            System.out.println("A velocidade da bola em " + planeta + " é: " + vT);
            System.out.println("A altura da bola em " + planeta + " é: " + hT);
        }
        else if (planeta.equals("Vênus")) {
            vT = v0 - (8.8 * t);
            hT = (v0 * t) - ((8.8 * (t * t)) / 2);
            System.out.println("A velocidade da bola em " + planeta + " é: " + vT);
            System.out.println("A altura da bola em " + planeta + " é: " + hT);
        }
        else if (planeta.equals("Terra")) {
            vT = v0 - (9.8 * t);
            hT = (v0 * t) - ((9.8 * (t * t)) / 2);
            System.out.println("A velocidade da bola na " + planeta + " é: " + vT);
            System.out.println("A altura da bola na " + planeta + " é: " + hT);
        }
        else if (planeta.equals("Marte")) {
            vT = v0 - (3.8 * t);
            hT = (v0 * t) - ((3.8 * (t * t)) / 2);
            System.out.println("A velocidade da bola em " + planeta + " é: " + vT);
            System.out.println("A altura da bola em " + planeta + " é: " + hT);
        }
        else if (planeta.equals("Júpiter")) {
            vT = v0 - (26.4 * t);
            hT = (v0 * t) - ((26.4 * (t * t)) / 2);
            System.out.println("A velocidade da bola em " + planeta + " é: " + vT);
            System.out.println("A altura da bola em " + planeta + " é: " + hT);
        }
        else if (planeta.equals("Sarturno")) {
            vT = v0 - (11.5 * t);
            hT = (v0 * t) - ((11.5 * (t * t)) / 2);
            System.out.println("A velocidade da bola em " + planeta + " é: " + vT);
            System.out.println("A altura da bola em " + planeta + " é: " + hT);
        }
        else if (planeta.equals("Urano")) {
            vT = v0 - (9.3 * t);
            hT = (v0 * t) - ((9.3 * (t * t)) / 2);
            System.out.println("A velocidade da bola em " + planeta + " é: " + vT);
            System.out.println("A altura da bola em " + planeta + " é: " + hT);
        }
        else if (planeta.equals("Netuno")) {
            vT = v0 - (12.2 * t);
            hT = (v0 * t) - ((12.2 * (t * t)) / 2);
            System.out.println("A velocidade da bola em " + planeta + " é: " + vT);
            System.out.println("A altura da bola em " + planeta + " é: " + hT);
        }
        else if (planeta.equals("Plutão")) {
            vT = v0 - (0.6 * t);
            hT = (v0 * t) - ((0.6 * (t * t)) / 2);
            System.out.println("A velocidade da bola em " + planeta + " é: " + vT);
            System.out.println("A altura da bola em " + planeta + " é: " + hT);
        }
        sc.close();
    }
}
