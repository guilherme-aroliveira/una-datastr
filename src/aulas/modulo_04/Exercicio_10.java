package aulas.modulo_04;

import java.util.Scanner;

public class Exercicio_10 {
    
    public static void main(String[] args) {
        
        String apartamento;
        int diarias; 
        double total;

        Scanner sc = new Scanner(System.in);

        System.out.print("Digite o tipo do apartamento hospedado: ");
        apartamento = sc.next();
        System.out.print("Digite o número de dias que ficou hospedado: ");
        diarias = sc.nextInt();

        sc.close();

        if (apartamento.equals("simples")) {

            if (diarias < 10) {
                total = diarias * 100;
                System.out.println("Total a ser pago: R$" + total);
            }
            else if (diarias >= 10 && diarias <= 15) {
                total = diarias * 90;
                System.out.println("Total a ser pago: R$" + total);
            }
            else {
                total = diarias * 80;
                System.out.println("Total a ser pago: R$" + total);
            }
        }
        else if (apartamento.equals("duplo")) {

            if (diarias < 10) {
                total = diarias * 140;
                System.out.println("Total a ser pago: R$" + total);
            }
            else if (diarias >= 10 && diarias <= 15) {
                total = diarias * 120;
                System.out.println("Total a ser pago: R$" + total);
            }
            else {
                total = diarias * 100;
                System.out.println("Total a ser pago: R$" + total);
            }
        }
    }
}
