package aulas.modulo_04;

import java.util.Scanner;

public class Exercicio_02 {
    
    public static void main(String[] args) {
        
        double nota1, nota2, nota3, media;

        Scanner sc = new Scanner(System.in);

        System.out.print("Digite a primeira nota: ");
        nota1 = sc.nextDouble();
        System.out.print("Digite a segunda nota: ");
        nota2= sc.nextDouble();
        System.out.print("Digite a terceira nota: ");
        nota3 = sc.nextDouble();

        sc.close();

        media = (nota1 + nota2 + nota3) / 3;

        if (media >= 0 && media < 3) {
            System.out.println("REPROVADO");
        }
        else if (media >= 3 && media < 7) {
            System.out.println("EXAME");
        }
        else if (media >= 7 && media <= 10) {
            System.out.println("APROVADO");
        }
    }
}
