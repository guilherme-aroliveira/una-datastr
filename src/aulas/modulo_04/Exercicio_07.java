package aulas.modulo_04;

import java.util.Scanner;

public class Exercicio_07 {
    
    public static void main(String[] args) {
        
        int num1, num2, opcao;

        Scanner sc = new Scanner(System.in);

        System.out.print("Digite um número inteiro: ");
        num1 = sc.nextInt();
        System.out.print("Digite um outro número inteiro: ");
        num2 = sc.nextInt();

        System.out.println("=====================");
        System.out.println("    MENU DE OPÇÕES   ");
        System.out.println("=====================");
        System.out.println("[1] - Somar os dois números");
        System.out.println("[2] - Multiplicar os dois números");
        System.out.println("[3] - Subtrair o maior pelo menor");
        System.out.println("[4] - Dividir o prrimeiro pleo segundo");

        System.out.print("Digite uma opção: ");
        opcao = sc.nextInt();
        System.out.println("---------------------");

        switch (opcao) {
            case 1:
                System.out.println("Soma: " + (num1 + num2));
                break;

            case 2:
                System.out.println("Multiplicação: " + (num1 * num2));
                break;

            case 3:
                if (num1 > num2) {
                    System.out.println("Subtração: " + (num1 - num2));
                }
                else if (num2 > num1) {
                    System.out.println("Subtração: " + (num2 - num1));
                }
                else {
                    System.out.println(0);
                }
                break;

            case 4:
                System.out.println("Divisão: " + (num1 / num2));
                break;
        
            default:
                System.out.println("Opção inválida!");
                break;
        }
        sc.close();
    }
}
