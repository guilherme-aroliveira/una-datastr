package aulas.modulo_04;

import java.util.Scanner;

public class Exercicio_08 {

    public static void main(String[] args) {
        
        int diarias; 
        double diariaValor, total;
        diariaValor = 500;

        Scanner sc = new Scanner(System.in);

        System.out.print("Digite o número de diarias: ");
        diarias = sc.nextInt();
        sc.close();

        if (diarias < 15) {
            total = (diariaValor * diarias) + (15 * diarias);
            System.out.println("Total da diaria: R$" + total);
        }
        else if(diarias == 15) {
            total = (diariaValor * diarias) + (10 * diarias);
            System.out.println("Total da diaria: R$" + total);
        }
        else {
            total = (diariaValor * diarias) + (5 * diarias);
            System.out.println("Total da diaria: R$" + total);
        }
    }
}
