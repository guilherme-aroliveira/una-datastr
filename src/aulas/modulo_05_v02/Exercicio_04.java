package aulas.modulo_05_v02;

import java.util.Scanner;

public class Exercicio_04 {
    
    public static void main(String[] args) {
        
        int num, cont=0;

        Scanner sc = new Scanner(System.in);

        for (int i = 1; i <= 10; i++) {

            System.out.print("Digite um número: ");
            num = sc.nextInt();

            if (num < 0) {
                cont++;
            }
        }
        sc.close();
        System.out.println("Foram lidos " + cont + " números negativos.");
    }
}
