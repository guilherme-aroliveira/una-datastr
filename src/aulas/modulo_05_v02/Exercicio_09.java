package aulas.modulo_05_v02;

import java.util.Scanner;

public class Exercicio_09 {
    
    public static void main(String[] args) {
        
        int num, maior=0, menor=0;

        Scanner sc = new Scanner(System.in);

        for (int i = 1; i <= 10; i++) {

            System.out.print("Digite um número: ");
            num = sc.nextInt();

            if (i == 1) {
                maior = num;
                menor = num;
            }
            else {
                if (num < menor) {
                    menor = num;
                }
                if (num > maior) {
                    maior = num;
                }
            }
        }
        sc.close();
        System.out.println("O maior valor foi: " + maior);
        System.out.println("O menor valor foi: " + menor);
        System.out.println();
    }
}
