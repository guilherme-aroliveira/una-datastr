package aulas.modulo_05_v02;

import java.util.Scanner;

public class Exercicio_15 {
    
    public static void main(String[] args) {
        
        double media;
        double nota1, nota2, soma;

        Scanner sc = new Scanner(System.in);

        for (int i = 1; i <= 3; i++) {
            System.out.print("Aluno " + i + " - Digite a primeira nota: ");
            nota1 = sc.nextDouble();
            System.out.print("Aluno " + i + " - Digite a segunda nota: ");
            nota2 = sc.nextDouble();

            soma = nota1 + nota2;
            media = soma / 2;

            System.out.println("A média do " + i + "° aluno: " + media);
        }
        sc.close();
        System.out.println();
    }
}
