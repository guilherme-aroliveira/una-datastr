package aulas.modulo_05_v02;

import java.util.Scanner;

public class Exercicio_08 {
    
    public static void main(String[] args) {
        
        int idade, soma=0, media;

        Scanner sc = new Scanner(System.in);

        for (int i = 1; i <= 10; i++) {

            System.out.print("Digite a sua idade: ");
            idade = sc.nextInt();

            soma = soma + idade;
        }
        sc.close();

        media = soma / 10;

        System.out.print("A média da idades é: " + media );
        System.out.println();
    }
}
