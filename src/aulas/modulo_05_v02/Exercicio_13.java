package aulas.modulo_05_v02;

import java.util.Scanner;

public class Exercicio_13 {
    
    public static void main(String[] args) {
        
        int idade, cont=0;
        String sexo;

        Scanner sc = new Scanner(System.in);

        for (int i = 1; i <= 10; i++) {

            System.out.print("Digite a suda idade: ");
            idade = sc.nextInt();

            System.out.print("Digite o seu sexo [M/F]: ");
            sexo = sc.next();

            if (sexo.equalsIgnoreCase("F") && (idade >= 20 && idade <= 40) ) {
                cont++;
            }
        }
        sc.close();
        System.out.println("\nHá " + cont + " mulheres entre 20 e 40 anos de idade.");
        System.out.println();
    }
}
