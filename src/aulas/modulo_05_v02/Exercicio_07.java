package aulas.modulo_05_v02;

import java.util.Scanner;

public class Exercicio_07 {
    
    public static void main(String[] args) {
        
        int num;
        
        Scanner sc = new Scanner(System.in);

        System.out.print("Digite um número: ");
        num = sc.nextInt();
        sc.close();

        for(int i = 1; i <= 10; i++) {
            System.out.println(num + " x " + i + " = " + (num * i) );
        }
        System.out.println();
    }
}
