package aulas.modulo_05_v02;

import java.util.Scanner;

public class Exercicio_16 {
    
    public static void main(String[] args) {
        
        double nota, maior=0, menor=0;

        Scanner sc = new Scanner(System.in);

        for (int i = 1; i <= 5; i++) {
            System.out.print("Aluno " + i + " - Digite a sua nota: ");
            nota = sc.nextDouble();

            if (i == 1) {
                maior = nota;
                menor = nota;
            }
            else {
                if (nota < menor) {
                    menor = nota;
                }
                if(nota > maior) {
                    maior = nota;
                }
            }
        }
        sc.close();
        System.out.println("--------------------------------");
        System.out.println("Maior nota: " + maior);
        System.out.println("Menor nota: " + menor);
    }
}
