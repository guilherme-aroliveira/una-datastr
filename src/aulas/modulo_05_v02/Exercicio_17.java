package aulas.modulo_05_v02;

import java.util.Scanner;

public class Exercicio_17 {
    
    public static void main(String[] args) {
        
        int idade, soma=0, media, quant=0;
        double peso;

        Scanner sc = new Scanner(System.in);

        for (int i = 1; i <= 7; i++) {

            System.out.print("Digite a sua idade: ");
            idade = sc.nextInt();

            System.out.print("Digite o seu peso: ");
            peso = sc.nextDouble();

            soma = soma + idade;

            if (peso > 90) {
                quant++;
            }
        }
        sc.close();

        media = soma / 7;

        System.out.println("-------------------------------------------");
        System.out.println("Número de pessos com mais de 90Kg: " + quant);
        System.out.println("A média das idades é: " + media);
    }
}
