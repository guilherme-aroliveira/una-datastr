package aulas.modulo_05_v02;

import java.util.Scanner;

public class Exercicio_11 {
    
    public static void main(String[] args) {
        
        int num1, num2, potencia=1;

        Scanner sc = new Scanner(System.in);

        System.out.print("Digite um número: ");
        num1 = sc.nextInt();
        System.out.print("Digite outro número: ");
        num2 = sc.nextInt();

        sc.close();

        for (int i = 1; i <= num2; i++) {
            potencia = potencia * num1;
        }
        System.out.println(num1 + "^" + num2 + " = " + potencia);
    }
}
