package aulas.modulo_05_v02;

import java.util.Scanner;

public class Exercicio_12 {
    
    public static void main(String[] args) {
        
        int num; 

        Scanner sc = new Scanner(System.in);

        System.out.print("Digite um número: ");
        num = sc.nextInt();

        sc.close();

        System.out.print("Valores pares: ");

        for (int i = 1; i <= num; i++) {

            if (i % 2 == 0) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
        
    }
}
