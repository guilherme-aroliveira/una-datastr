package aulas.modulo_05_v02;

public class Exercicio_03 {
    
    public static void main(String[] args) {
        
        for (int i = 1; i <= 10; i++) {

            if (i % 2 == 0) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }
}
