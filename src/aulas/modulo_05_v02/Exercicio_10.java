package aulas.modulo_05_v02;

import java.util.Scanner;

public class Exercicio_10 {
    
    public static void main(String[] args) {
        
        int num, fat=1;

        Scanner sc = new Scanner(System.in);

        System.out.print("Digite um valor: ");
        num = sc.nextInt();

        sc.close();

        for (int i = 1; i <= num; i++) {

            fat = fat * i; 
        }
        System.out.println("Fatorial de !" + num + " = " + fat);
        System.out.println();

    }
}
