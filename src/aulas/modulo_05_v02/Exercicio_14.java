package aulas.modulo_05_v02;

import java.util.Scanner;

public class Exercicio_14 {
    
    public static void main(String[] args) {
        
        String sexo;
        double peso;
        int homens=0, mulheres=0;

        Scanner sc = new Scanner(System.in);

        for (int i = 1; i <= 10; i++) {

            System.out.print("Digite o seu sexo [M/F]: ");
            sexo = sc.next();

            System.out.print("Digite o seu peso: ");
            peso = sc.nextDouble();

            if (sexo.equalsIgnoreCase("M") && (peso >= 60 && peso <= 80)) {
                homens++;
            }
            else if (sexo.equalsIgnoreCase("F") && (peso >= 50 && peso <= 70) ) {
                mulheres++;
            }
        }
        sc.close();
        System.out.println("Há " + homens + " homens com o peso entre 60 e 80kg");
        System.out.println("Há " + mulheres + " mulheres com o peso entre 50 e 70kg");
    }
}
