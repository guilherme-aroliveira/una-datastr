package aulas.modulo_05_v01;

import java.util.Scanner;

public class Exercicio_03 {
    
    public static void main(String[] args) {
        
        String nome, senha;

        Scanner sc = new Scanner(System.in);

        do {
            System.out.print("Digite o seu nome: ");
            nome = sc.nextLine();
            
            System.out.print("Digite a sua senha: ");
            senha = sc.nextLine();

            if (nome.equalsIgnoreCase(senha)) {
                System.out.println("O nome não pode ser igual à senha!!");
            }

        } while (nome.equalsIgnoreCase(senha));
        sc.close();
    }
}
