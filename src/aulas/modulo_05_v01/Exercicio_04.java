package aulas.modulo_05_v01;

import java.util.Scanner;

public class Exercicio_04 {
    
    public static void main(String[] args) {
        
        int num1, num2, opcao;

        Scanner sc = new Scanner(System.in);

        System.out.print("Digite um número inteiro: ");
        num1 = sc.nextInt();
        System.out.print("Digite outro número inteiro: ");
        num2 = sc.nextInt();

        do {

            System.out.println("=====================");
            System.out.println("    MENU DE OPÇÕES   ");
            System.out.println("=====================");
            System.out.println("[1] - Somar "
                + "\n[2] - Subtrair"
                + "\n[3] - Multiplicar"
                + "\n[4] - Dividir"
                + "\n[5] - Sair"
            );

            System.out.print("Escolha um opção: ");
            opcao = sc.nextInt();
            System.out.println("---------------------");

            switch (opcao) {
                case 1:
                    System.out.println("Valor da soma: " + (num1 + num2));
                    break;

                case 2:
                    if (num1 > num2) {
                        System.out.println("Valor da subtração: " + (num1 - num2));
                    }
                    else if (num2 > num1) {
                        System.out.println("Valor da subtração: " + (num2 - num1));
                    }
                    else {
                        System.out.println("Valor da subtração: " + (num1 - num2));
                    }
                    break;

                case 3:
                    System.out.println("Valor da multiplicação: " + (num1 * num2));
                    break;

                case 4:
                    if (num1 > num2) {
                        System.out.println("Valor da divisão: " + (num1 / num2));
                    }
                    else if (num2 > num1) {
                        System.out.println("Valor da divisão: " + (num2 / num1));
                    }
                    break;

                case 5:
                    System.out.println("Fim do programa!");
                    break;
            
                default:
                    System.out.println("Opção inválida");
                    break;
            }

        } while (opcao != 5);
        sc.close();
    }
}
