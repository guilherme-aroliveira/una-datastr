package aulas.modulo_05_v01;

import java.util.Scanner;

public class Exercicio_01 {
    
    public static void main(String[] args) {
        
        double base, altura, area;

        Scanner sc = new Scanner(System.in);

        do {
            System.out.print("Digite a base do triângulo: ");
            base = sc.nextDouble();

            if (base < 0) {
                System.out.println("Base inválida.");
            }

        } while (base < 0);
        
        do {
            System.out.print("Digite a altura do triângulo: ");
            altura = sc.nextDouble();

            if (altura < 0) {
                System.out.println("Altura inválida.");
            }
        } while (altura < 0);
        sc.close();

        area = (base * altura) / 2;

        System.out.println("A área do trigângulo é: " + area);
    }
}
