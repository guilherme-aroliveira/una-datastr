package aulas.modulo_05_v01;

import java.util.Scanner;

public class Exercicio_02 {
    
    public static void main(String[] args) {
        
        int num;
        String resp;

        Scanner sc = new Scanner(System.in);

        do {
            System.out.print("Digite um número inteiro qualquer: ");
            num = sc.nextInt();

            System.out.println("O dobro de " + num + " é " + (num* 2));

            System.out.print("Deseja continuar [S/N]: ");
            resp = sc.next();

        } while (resp.equalsIgnoreCase("S"));
        sc.close();
    }
}
