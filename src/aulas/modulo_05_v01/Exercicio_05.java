package aulas.modulo_05_v01;

import java.util.Scanner;

public class Exercicio_05 {
    
    public static void main(String[] args) {
        
        double nota, salario;
        String sexo;
        int idade;
        
        Scanner sc = new Scanner(System.in);

        do {
            System.out.print("Digite sua nota: ");
            nota = sc.nextDouble();

            if (nota < 0 || nota > 10) {
                System.out.println("Nota inválida. Digite novamente!");
            }

        } while (nota < 0 || nota > 10);

        do {
            System.out.print("Digite o seu salário: ");
            salario = sc.nextDouble();

            if (salario <= 0) {
                System.out.println("Salário inválido. Digite novamente!");
            }

        } while (salario <= 0);

        do {
            System.out.print("Digite o seu sexo [M/F]: ");
            sexo = sc.next();

            if ( !(sexo.equalsIgnoreCase("M")) && !(sexo.equalsIgnoreCase("F")) ) {
                System.out.println("Sexo inválido. Digite novamente!");
            }

        } while ( !(sexo.equalsIgnoreCase("M")) && !(sexo.equalsIgnoreCase("F")) );

        do {
            System.out.print("Digite a sua idade: ");
            idade = sc.nextInt();

            if (idade < 0 || idade > 150) {
                System.out.println("Idade inválida. Digite novamente!");
            }

        } while (idade < 0 || idade > 150);

        System.out.println("Fim do programa!");
        sc.close();
    }
}
